*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${JIRA_SERVER}    streamline.di.net.au:8080
${WIKI_SERVER}    streamline.di.net.au:8090
${BROWSER}        Chrome
${DELAY}          0
${VALID USER}     smoke
${VALID PASSWORD}    smokeit
${JIRA_LOGIN URL}    http://${JIRA_SERVER}/
${WIKI_LOGIN URL}    http://${WIKI_SERVER}/
${JIRA_WELCOME URL}    http://${JIRA_SERVER}/secure/Dashboard.jspa
${JIRA_ERROR URL}    http://${JIRA_SERVER}/secure/Dashboard.jspa
${WIKI_WELCOME URL}    http://${WIKI_SERVER}/index.action#all-updates
${WIKI_ERROR URL}    http://${WIKI_SERVER}/dologin.action
${NONVALID USER}    invalid
${NONVALID PASSWORD}    invalid
${RISK SUMMARY}    Test risk for RobotFramework
${BUG SUMMARY}    Test bug for RobotFramework
${StreamLine Project Name}    SSDC
${Default Summary}    Testing by RobotFramework

*** Keywords ***
Set Environment Variable
    webdriver.chrome.driver    chromedriver.exe

Open JIRA To Login Page
    Open Browser    ${JIRA_LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    JIRA Login Page Should Be Open

JIRA Login Page Should Be Open
    Page Should Contain    Dashboards

Go To JIRA Login Page
    Go To    ${JIRA_LOGIN URL}
    JIRA Login Page Should Be Open

Enter JIRA Username
    [Arguments]    ${username}
    Input Text    id=login-form-username    ${username}

Enter JIRA Password
    [Arguments]    ${password}
    Input Password    id=login-form-password    ${password}

Submit JIRA Credentials
    Click Button    Log In

JIRA Welcome Page Should Be Open
    Location Should Be    ${JIRA_WELCOME URL}
    Page Should Contain    Dashboards

Open Confluence To Login Page
    Open Browser    ${WIKI_LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    WIKI Login Page Should Be Open

WIKI Welcome Page Should Be Open
    Location Should Be    ${WIKI_WELCOME URL}
    Page Should Contain    All updates

Go To WIKI Login Page
    Go To    ${WIKI_LOGIN URL}
    WIKI Login Page Should Be Open

WIKI Login Page Should Be Open
    Page Should Contain    Log in

Enter WIKI Username
    [Arguments]    ${username}
    Input Text    id=os_username    ${username}

Enter WIKI Password
    [Arguments]    ${password}
    Input Password    id=os_password    ${password}

Submit WIKI Credentials
    Click Button    Log in

Login to Streamline JIRA
    Open JIRA To Login Page
    Enter JIRA Username    ${VALID USER}
    Enter JIRA Password    ${VALID PASSWORD}
    Submit JIRA Credentials

Click Project Field
    Click Element    xpath=//a[@id='create_link']

Fill Project Field
    Input Text    id=project-field    ${StreamLine Project Name}
    Press Key    id=project-field    \\13
    Sleep    1 s

Summary Field
    Input Text    id=summary    ${Default Summary}
