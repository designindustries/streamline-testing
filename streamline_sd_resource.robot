*** Settings ***
Library           Selenium2Library
Resource          resource.robot

*** Variables ***
@{ISSUETYPE}      Change

*** Keywords ***
ISSUETYPE_BUG
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    bug
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Just Checking
    Run Keyword And Continue On Failure    Input Text    id=priority-field    minor
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_10300']//option[5]
    Run Keyword And Continue On Failure    Input Text    id=description    Nothin to see here
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    reporting
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    //select[@id='customfield_17600:1']//option[2]

ISSUETYPE_ISSUE
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    issue
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Just Checking
    Run Keyword And Continue On Failure    Input Text    id=description    Nothin to see here
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    reporting
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    //select[@id='customfield_17600:1']//option[2]

ISSUETYPE_RISK
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    risk
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Something
    Run Keyword And Continue On Failure    Input Text    id=description    ROBO wars with risks
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_14903']//option[3]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_14904']//option[5]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_15700']//option[2]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_15800']//option[6]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_15801']//option[2]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_15006']//option[3]
    Run Keyword And Continue On Failure    Click Element    xpath=//div[@id='components-multi-select']//span[.='More']
    Run Keyword And Continue On Failure    Click Element    link=User Interface
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Run Keyword And Continue On Failure    Click Element    xpath=//tr[@class='headrow']//div[.='›']
    Run Keyword And Continue On Failure    Click Element    xpath=//div[5]/table/tbody/tr[4]/td[5]
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[2]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]

ISSUETYPE_DECISION
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    decision
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Something
    Run Keyword And Continue On Failure    Input Text    id=description    ROBO deciding
    Run Keyword And Continue On Failure    Input Text    id=customfield_14700    No decision made
    Run Keyword And Continue On Failure    Click Element    //div[@id='priority-single-select']//span[.='More']
    Run Keyword And Continue On Failure    Click Element    xpath=//div[5]/div/div/ul/li[8]/a
    Run Keyword And Continue On Failure    Click Element    xpath=//div[@id='components-multi-select']//span[.='More']
    Run Keyword And Continue On Failure    Click Element    link=User Interface
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Run Keyword And Continue On Failure    Click Element    xpath=//div[6]/table/tbody/tr[5]/td[4]
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Log    Field: Phase
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_16500']//option[8]
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Log    Field:Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[2]
    Log    Field:Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[3]

ISSUETYPE_EPIC
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    epic
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    2 s
    Log    Field - Epic Name
    Run Keyword And Continue On Failure    Input Text    id=customfield_12201    Robo Epic
    Run Keyword And Continue On Failure    Input Text    id=summary    ROBO summary
    Run Keyword And Continue On Failure    Input Text    id=description    ROBO deciding
    Run Keyword And Continue On Failure    Click Element    //div[@id='priority-single-select']//span[.='More']
    Run Keyword And Continue On Failure    Click Element    xpath=//div[5]/div/div/ul/li[8]/a
    Run Keyword And Continue On Failure    Input Text    id=timetracking_originalestimate    12h
    Run Keyword And Continue On Failure    Input Text    id=timetracking_remainingestimate    10h
    Run Keyword And Continue On Failure    Click Element    xpath=//div[@id='components-multi-select']//span[.='More']
    Run Keyword And Continue On Failure    Click Element    link=User Interface
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Business Value
    Run Keyword And Continue On Failure    Input Text    id=customfield_10106    yeah why not
    Sleep    1 s
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[2]
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[3]

ISSUETYPE_FEATURE
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    feature
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    ROBO summary
    Run Keyword And Continue On Failure    Input Text    id=priority-field    Critical
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    User interface
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Run Keyword And Continue On Failure    Input Text    id=description    Something awesome
    Run Keyword And Continue On Failure    Input Text    id=timetracking_originalestimate    10h
    Run Keyword And Continue On Failure    Input Text    id=timetracking_remainingestimate    12h
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Date
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Log    Field : Business Value
    Run Keyword And Continue On Failure    Input Text    id=customfield_10106    values are important
    Run Keyword And Continue On Failure    Press Key    id=customfield_10106    \\13
    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=customfield_14908    success is near
    Run Keyword And Continue On Failure    Press Key    id=customfield_14908    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]

ISSUETYPE_CHANGE
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    change
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    2 s
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Summary Field
    Run Keyword And Continue On Failure    Input Text    id=customfield_16901    smoke
    Sleep    1 s    #Remove this and the next Line CREATES the issue ignoring the rest
    Run Keyword And Continue On Failure    Press Key    id=customfield_16901    \\13
    Run Keyword And Continue On Failure    Input Text    id=priority-field    Minor
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Log    Field: Impacted Project
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_16800']//option[22]
    Run Keyword And Continue On Failure    Input Text    id=environment    nice
    Run Keyword And Continue On Failure    Input Text    id=description    test description
    Log    Field: Justification
    Run Keyword And Continue On Failure    Input Text    id=customfield_14804    cannot
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    User interface
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Log    Field: Linked Issues
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='issuelinks-linktype']//option[6]
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Log    Just to make date box disappear
    Run Keyword And Continue On Failure    Click Element    id=components-textarea
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]

ISSUETYPE_DELIVERABLE
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    deliverable
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    2 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Delivering the deliverable
    Run Keyword And Continue On Failure    Input Text    id=description    testing description
    Log    Field: Phase
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_16500']//option[4]
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Run Keyword And Continue On Failure    Input Text    id=priority-field    Minor
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    User interface
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Run Keyword And Continue On Failure    Input Text    id=timetracking_originalestimate    10h
    Run Keyword And Continue On Failure    Input Text    id=timetracking_remainingestimate    12h
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]

ISSUETYPE_ChangeRequest
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    change request
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Changing the request
    Run Keyword And Continue On Failure    Input Text    id=description    testing description
    Log    Field: Justification
    Run Keyword And Continue On Failure    Input Text    id=customfield_14804    cannot
    Run Keyword And Continue On Failure    Input Text    id=priority-field    Minor
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    User interface
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Log    Just to make date box disappear
    Run Keyword And Continue On Failure    Click Element    id=components-textarea
    Log    Field: Decision Made
    Run Keyword And Continue On Failure    Input Text    id=customfield_14700    not really
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Log    Field: Business Value
    Run Keyword And Continue On Failure    Input Text    id=customfield_10106    yeah why not
    Sleep    1 s
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]

ISSUETYPE_STORY
    Run Keyword And Continue On Failure    Input Text    id=project-field    teams f
    Run Keyword And Continue On Failure    Press Key    id=project-field    \\13
    Run Keyword And Continue On Failure    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=issuetype-field    Story
    Run Keyword And Continue On Failure    Press Key    id=issuetype-field    \\13
    Sleep    1 s
    Run Keyword And Continue On Failure    Input Text    id=summary    Story made awesome
    Run Keyword And Continue On Failure    Input Text    id=priority-field    Minor
    Run Keyword And Continue On Failure    Press Key    id=priority-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=components-textarea    User interface
    Run Keyword And Continue On Failure    Press Key    id=components-textarea    \\13
    Log    Field: Sprint
    Run Keyword And Continue On Failure    Input Text    id=customfield_11400-field    active
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_11400-field    \\13
    Run Keyword And Continue On Failure    Click Element    link=Select a date
    Log    Just to make date box disappear
    Run Keyword And Continue On Failure    Click Element    id=summary
    Run Keyword And Continue On Failure    Input Text    id=timetracking_originalestimate    10h
    Run Keyword And Continue On Failure    Input Text    id=timetracking_remainingestimate    12h
    Run Keyword And Continue On Failure    Click Element    id=assign-to-me-trigger
    Run Keyword And Continue On Failure    Input Text    id=description    testing description
    Log    Field: Business Value
    Run Keyword And Continue On Failure    Input Text    id=customfield_10106    yeah why not
    Sleep    1 s
    Log    Field: Story Points
    Run Keyword And Continue On Failure    Input Text    id=customfield_10105    nope
    Sleep    1 s
    Log    Field: Success Criteria
    Run Keyword And Continue On Failure    Input Text    id=customfield_14908    Success is relative
    Sleep    1 s
    Log    Field: Epic Link
    Run Keyword And Continue On Failure    Input Text    id=customfield_12200-field    feature
    Sleep    1 s
    Run Keyword And Continue On Failure    Press Key    id=customfield_12200-field    \\13
    Run Keyword And Continue On Failure    Input Text    id=labels-textarea    test
    Run Keyword And Continue On Failure    Press Key    id=labels-textarea    \\13
    Log    Field: Workstream Part 1
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Log    Field: Workstream Part 2
    Run Keyword And Continue On Failure    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]
