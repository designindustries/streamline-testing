*** Settings ***
Documentation     A test suite to test the Streamline Risk workflow in JIRA.
Resource          ../../resource.robot

*** Test Cases ***
Log into JIRA
    Open JIRA To Login Page
    Enter JIRA Username    ${VALID USER}
    Enter JIRA Password    ${VALID PASSWORD}
    Submit JIRA Credentials
    JIRA Welcome Page Should Be Open
    Wait Until Page Contains    Boards

Create a Risk
    Click Element    xpath=//a[@id='create_link']
    Wait Until Element Is Visible    id=project-field
    Input Text    id=project-field    teams f
    Press Key    id=project-field    \\13
    Sleep    1 second
    Input Text    id=issuetype-field    risk
    Press Key    id=issuetype-field    \\13
    Sleep    1 second
    Input Text    id=summary    ${RISK SUMMARY}
    Input Text    id=description    This is an example test. woohoo
    Click Element    xpath=//select[@id='customfield_14903']//option[3]
    Click Element    xpath=//select[@id='customfield_14904']//option[5]
    Click Element    xpath=//select[@id='customfield_15700']//option[2]
    Click Element    xpath=//select[@id='customfield_15800']//option[6]
    Click Element    xpath=//select[@id='customfield_15801']//option[2]
    Click Element    xpath=//select[@id='customfield_15006']//option[3]
    Click Element    id=assign-to-me-trigger
    Click Element    link=Select a date
    Click Element    xpath=//tr[@class='headrow']//div[.='›']
    Click Element    xpath=//div[5]/table/tbody/tr[4]/td[5]
    Click Element    xpath=//select[@id='customfield_17600']//option[2]
    Click Element    xpath=//select[@id='customfield_17600:1']//option[2]
    Click Element    id=create-issue-submit
    Sleep    1 second
    Click Element    xpath=//a[@class='issue-created-key issue-link']
    Page Should Contain    ${RISK SUMMARY}
    Page Should Contain    (( 8 ))

Transition Risk
    Page Should Contain    ${RISK SUMMARY}
    Run Keyword And Ignore Error    Click Link    Reopen
    Run Keyword And Ignore Error    Wait Until Page Contains    Mitigated
    Click Link    Mitigated
    Sleep    1 second
    Click Element    xpath=//select[@id='resolution']//option[7]
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Reopen
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Eventuated
    Sleep    1 second
    Click Element    xpath=//select[@id='resolution']//option[7]
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Wait Until Page Contains    ${RISK SUMMARY} - Risk Realised
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Reopen
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Eventuated - ongoing
    Sleep    1 second
    Capture Page Screenshot
