*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../resource.robot

*** Test Cases ***
Valid Login
    Open JIRA To Login Page
    Enter JIRA Username    ${VALID USER}
    Enter JIRA Password    ${VALID PASSWORD}
    Submit JIRA Credentials
    JIRA Welcome Page Should Be Open
    [Teardown]    Close Browser
