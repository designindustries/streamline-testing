*** Settings ***
Documentation     A test suite containing tests related to invalid login.
...
...               These tests are data-driven by their nature. They use a single
...               keyword, specified with Test Template setting, that is called
...               with different arguments to cover different scenarios.
...
...               This suite also demonstrates using setups and teardowns in
...               different levels.
Suite Setup       Open JIRA To Login Page
Suite Teardown    Close Browser
Test Setup        Go To JIRA Login Page
Test Template     Login With Invalid Credentials Should Fail
Resource          ../../resource.robot

*** Test Cases ***    USER NAME           PASSWORD
Invalid Username      invalid             ${NONVALID PASSWORD}

Invalid Password      ${NONVALID USER}    invalid

Invalid Username And Password
                      ${NONVALID USER}    ${NONVALID PASSWORD}

Empty Username        ${EMPTY}            ${NONVALID PASSWORD}

Empty Password        ${NONVALID USER}    ${EMPTY}

Empty Username And Password
                      ${EMPTY}            ${EMPTY}

*** Keywords ***
Login With Invalid Credentials Should Fail
    [Arguments]    ${username}    ${password}
    Enter JIRA Username    ${username}
    Enter JIRA Password    ${password}
    Submit JIRA Credentials
    Wait Until Page Contains    Sorry
    Login Should Have Failed

Login Should Have Failed
    Page Should Contain    Sorry
