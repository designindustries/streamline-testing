*** Settings ***
Documentation     A test suite to test the Streamline Risk workflow in JIRA.
Resource          ../../resource.robot

*** Test Cases ***
Log into JIRA
    Open JIRA To Login Page
    Enter JIRA Username    ${VALID USER}
    Enter JIRA Password    ${VALID PASSWORD}
    Submit JIRA Credentials
    JIRA Welcome Page Should Be Open
    Wait Until Page Contains    Boards

Create a Bug
    Click Element    xpath=//a[@id='create_link']
    Wait Until Element Is Visible    id=project-field
    Input Text    id=project-field    teams f
    Press Key    id=project-field    \\13
    Sleep    1 second
    Input Text    id=issuetype-field    bug
    Press Key    id=issuetype-field    \\13
    Sleep    1 second
    Input Text    id=summary    ${BUG SUMMARY}
    Click Element    xpath=//select[@id='customfield_10300']//option[5]
    Input Text    id=description    This is an example test. woohoo
    Input Text    id=customfield_10308    Step 1. Click on link\nStep 2. Link should appear
    Input Text    id=environment    Staging server
    Click Element    xpath=//select[@id='customfield_17600']//option[3]
    Click Element    xpath=//select[@id='customfield_17600:1']//option[3]
    Click Element    id=create-issue-submit
    Sleep    1 second
    Click Element    xpath=//a[@class='issue-created-key issue-link']
    Page Should Contain    ${BUG SUMMARY}

Transition Bug
    Page Should Contain    ${BUG SUMMARY}
    Click Link    Place in Backlog
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Analyse Defect
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Ready for Development
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Start Development
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Review with Team
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Complete Review
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Ready for Testing
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Start Testing
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Test Failed
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Start Development
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Ready for Testing
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Start Testing
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Test Passed
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Close Defect
    Sleep    1 second
    Click Element    xpath=//select[@id='resolution']//option[7]
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Sleep    1 second
    Capture Page Screenshot
    Click Link    Reopen Defect
    Sleep    1 second
    Click Element    xpath=//input[@id='issue-workflow-transition-submit']
    Capture Page Screenshot
    Page Should Contain    Has Defect Being Tested Previously?
