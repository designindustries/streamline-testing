*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../resource.robot
Resource          ../../streamline_teams_resource.robot

*** Test Cases ***
Check Fields for ISSUETYPES
    : FOR    ${ISSUE}    IN    @{ISSUETYPE}
    \    Open JIRA To Login Page
    \    Enter JIRA Username    ${VALID USER}
    \    Enter JIRA Password    ${VALID PASSWORD}
    \    Submit JIRA Credentials
    \    Wait Until Page Contains    Boards
    \    Click Element    xpath=//a[@id='create_link']
    \    Wait Until Element Is Visible    id=project-field
    \    Run Keyword If    '${ISSUE}'=='Bug'    ISSUETYPE_BUG
    \    Run Keyword If    '${ISSUE}'=='Issue'    ISSUETYPE_ISSUE
    \    Run Keyword If    '${ISSUE}'=='Risk'    ISSUETYPE_RISK
    \    Run Keyword If    '${ISSUE}'=='Decision'    ISSUETYPE_DECISION
    \    Run Keyword If    '${ISSUE}'=='Epic'    ISSUETYPE_EPIC
    \    Run Keyword If    '${ISSUE}'=='Feature'    ISSUETYPE_FEATURE
    \    Run Keyword If    '${ISSUE}'=='Change'    ISSUETYPE_CHANGE
    \    Run Keyword If    '${ISSUE}'=='Deliverable'    ISSUETYPE_Deliverable
    \    Run Keyword If    '${ISSUE}'=='Story'    ISSUETYPE_Story
    \    Run Keyword If    '${ISSUE}'=='ChangeRequest'    ISSUETYPE_CHANGEREQUEST
    \    Close Browser    #Closing
