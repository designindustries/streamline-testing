*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../../resource.robot
Resource          ../../streamline_sd_resource.robot

*** Test Cases ***
Check Fields for ISSUETYPES
    : FOR    ${ISSUE}    IN    @{ISSUETYPE}
    \    Login to Streamline JIRA
    \    Wait Until Page Contains    Boards
    \    Click Project Field
    \    Wait Until Element Is Visible    id=project-field
    \    Fill Project Field
    \    Run Keyword If    '${ISSUE}'=='Change'    ISSUETYPE_CHANGE
    \    \    #Closing
